cbuffer WorldTransforms : register(b0)
{
	matrix World;
	matrix Dlrow;
};

cbuffer ViewTransforms : register(b1)
{
	matrix View;
};

cbuffer ProjectionTransforms : register(b2)
{
	matrix Projection;
};

cbuffer ViewTransforms : register(b3)
{
	matrix LightView;
};

cbuffer ProjectionTransforms : register(b4)
{
	matrix LightProjection;
};


struct VertexShaderInput
{
	float3 pos : SV_POSITION;
};

struct VertexShaderOutput
{
	float4 pos : SV_POSITION;
};


VertexShaderOutput main(VertexShaderInput input)
{
	VertexShaderOutput output;

	// Change the position vector to be 4 units for proper matrix calculations.
	float4 pos = float4(input.pos, 1.0f);;
	
	// Calculate the position of the vertex against the world, view, and projection matrices.
	pos = mul(pos, World);
	pos = mul(pos, LightView);
	pos = mul(pos, LightProjection);

	output.pos = pos;

	return output;
}