#include "..\..\Engine\Lighting\HLSL\Directional Light.hlsli"

Texture2D shaderTexture : register(t0);
Texture2D depthMapTexture : register(t1);

SamplerState SampleTypeWrap  : register(s0);
SamplerState SampleTypeClamp : register(s1);
SamplerComparisonState SampleTypeCompare : register(s2);


cbuffer MaterialBuffer : register(b0)
{
	Material material;
};

cbuffer CameraPosition : register(b1)
{
	float3 Eye;
};

cbuffer DirectionalLightBuffer : register (b2)
{
	DirectionalLight directional;
};

cbuffer TextureSize : register(b3)
{
	float textureWidth;
	float textureHeight;
}

struct PixelShaderInput
{
	float4 position : SV_POSITION;
	float3 worldNormal : NORMAL;
	float2 tex : TEXCOORD0;
	float4 lightViewPosition : TEXCOORD1;
	float3 lightPos : TEXCOORD2;
	float3 worldPosition : POSITION;
};

float2 texOffset(int u, int v)
{
	return float2(u * 1.0f / textureWidth, v * 1.0f / textureHeight);
}

float4 main(PixelShaderInput input) : SV_Target
{
	

	float bias;
	float2 projectTexCoord;
	float depthValue;
	float lightDepthValue;
	float4 color;
	float4 colorTex;
	float4 lightPosition = input.lightViewPosition;

	// Set the value for fixing the floating point precision issues.
	bias = 0.001f;

	// Interpolating normal can unnormalize it, so normalize it
	input.worldNormal = normalize(input.worldNormal);
	
	normalize(directional.Direction);

	float3 tocamera = normalize(Eye - input.worldPosition);

		// Start with a sum of zero. 
		float4 ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
		float4 diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
		float4 specular = float4(0.0f, 0.0f, 0.0f, 0.0f);

		float4 a, d, s;

	

	ComputeDirectionalLight(material, directional, input.worldNormal, tocamera, a, d, s);
	ambient += a;
	diffuse += d;
	specular += s;

	
	color = shaderTexture.Sample(SampleTypeWrap, input.tex);
	
	input.lightViewPosition.xyz /= input.lightViewPosition.w;
	
	
	if (input.lightViewPosition.x < -1.0f || input.lightViewPosition.x > 1.0f ||
		input.lightViewPosition.y < -1.0f || input.lightViewPosition.y > 1.0f ||
		input.lightViewPosition.z < 0.0f || input.lightViewPosition.z > 1.0f) return color * ambient;

	input.lightViewPosition.x = input.lightViewPosition.x / 2 + 0.5;
	input.lightViewPosition.y = input.lightViewPosition.y / -2 + 0.5;

	input.lightViewPosition.z -= bias;

	//PCF sampling for shadow map
	float sum = 0;
	float x = 0, y = 0;

	//perform PCF filtering on a 8 x 8 texel neighborhood
	for (y = -3.5; y <= 3.5; y += 1.0)
	{
		for (x = -3.5; x <= 3.5; x += 1.0)
		{
			sum += depthMapTexture.SampleCmpLevelZero(SampleTypeCompare, input.lightViewPosition.xy + texOffset(x, y), input.lightViewPosition.z);
		
		}
	}

	//depthValue = depthMapTexture.Sample(SampleTypeClamp, input.lightViewPosition.xy).r;
	
	//sum += depthMapTexture.SampleCmpLevelZero(SampleTypeCompare, input.lightViewPosition.xy + texOffset(x, y), input.lightViewPosition.z);

	//lightDepthValue = input.lightViewPosition.z / input.lightViewPosition.w;

	//float shadowFactor = input.lightViewPosition.z <= depthValue;

	float shadowFactor = sum / 64.0f;


	float3 L = input.lightPos.xyz;
		float ndotl = dot(normalize(input.worldNormal), -L);
	return color * (ambient + diffuse * ndotl * shadowFactor) + specular * ndotl * shadowFactor;

	// Common to take alpha from diffuse material.
	color.a = material.Diffuse.a;

	return color;
}