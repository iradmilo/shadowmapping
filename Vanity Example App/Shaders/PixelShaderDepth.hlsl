struct PixelShaderInput
{
	float4 pos : SV_POSITION;
};

float4 main(PixelShaderInput input) : SV_Target
{	
	float color;

	float depthValue = input.pos.z;
	
	color = float4(depthValue, depthValue, depthValue, 1.0f);
	

	return color;
}
