
cbuffer WorldTransforms : register(b0)
{
	matrix World;
	matrix Dlrow;
};

cbuffer ViewTransforms : register(b1)
{
	matrix View;
};

cbuffer ProjectionTransforms : register(b2)
{
	matrix Projection;
};

cbuffer ViewTransforms : register(b3)
{
	matrix LightView;
};

cbuffer ProjectionTransforms : register(b4)
{
	matrix LightProjection;
};

cbuffer LightBuffer : register (b5)
{
	float3 LightPosition;
	float Padding;
};

struct VertexShaderInput
{
	float3 position : SV_POSITION;
	float3 normal : NORMAL;
	float2 tex : TEXCOORD0;
};

struct VertexShaderOutput
{
	float4 position : SV_POSITION;
	float3 worldNormal : NORMAL;
	float2 tex : TEXCOORD0;
	float4 lightViewPosition : TEXCOORD1;
	float3 lightPos : TEXCOORD2;
	float3 worldPosition : POSITION;
};

VertexShaderOutput main(VertexShaderInput input)
{
	VertexShaderOutput output;

	// Transform to world space 
	// Calculate the position of the vertex in the world.
	output.worldPosition = mul(float4(input.position, 1.0f), World).xyz;


	// Transform to clip space 
	float4 pos = float4(input.position, 1.0f);

	// Calculate the position of the vertex against the world, view, and projection matrices.
	pos = mul(pos, World);
	pos = mul(pos, View);
	pos = mul(pos, Projection);

	output.position = pos;


	//output.worldNormal = mul(input.normal, (float3x3)World);
	output.worldNormal = normalize(mul(input.normal, (float3x3)Dlrow));

	// Calculate the position of the vertice as viewed by the light source.
	float4 lightViewPos = float4(input.position, 1.0f);

	lightViewPos = mul(lightViewPos, World);
	lightViewPos = mul(lightViewPos, LightView);
	lightViewPos = mul(lightViewPos, LightProjection);

	output.lightViewPosition = lightViewPos;

	// Store the texture coordinates for the pixel shader.
	output.tex = input.tex;

	// Determine the light position based on the position of the light and the position of the vertex in the world.
	output.lightPos = LightPosition.xyz - output.worldPosition.xyz;

	// Normalize the light position vector.
	output.lightPos = normalize(output.lightPos);

	return output;
}