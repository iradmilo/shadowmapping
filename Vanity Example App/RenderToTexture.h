#pragma once
#include "pch.h"

class RenderTextureClass{
public:
	RenderTextureClass();

	void Initialize(std::shared_ptr<DX::DeviceResources>, float, float);
	void Reset();

	void ClearRenderTarget(std::shared_ptr<DX::DeviceResources>, float, float, float, float);
	void SetRenderTarget(std::shared_ptr<DX::DeviceResources>);
	ID3D11ShaderResourceView* GetShaderResourceView();
	ID3D11DepthStencilView* RenderTextureClass::GetDepthStencilView();
	ID3D11Texture2D* GetRenderTargetTexture();


private:
	Microsoft::WRL::ComPtr<ID3D11DeviceContext2> m_context;
	Microsoft::WRL::ComPtr<ID3D11Device2> m_device;
	Microsoft::WRL::ComPtr<ID3D11Texture2D> m_renderTargetTexture;
	Microsoft::WRL::ComPtr<ID3D11Texture2D> m_shadowTexture;
	Microsoft::WRL::ComPtr<ID3D11Texture2D> m_depthStencilBuffer;
	Microsoft::WRL::ComPtr<ID3D11RenderTargetView> m_renderTargetView;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_shaderResourceView;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_shaderResourceView1;
	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> pShadowMapSRView;
	Microsoft::WRL::ComPtr<ID3D11DepthStencilView> m_depthStencilView;
	D3D11_VIEWPORT m_viewport;
};