
#pragma once

//
//	Renderer3D.h
//  Defining a 3D renderer
//
//  � 2015 Vanity DirectX 11.2 Engine (VXE). Zoraja Consulting d.o.o. All rights reserved.
//

#include "pch.h"

#include "..\Engine\Third Party\DirectX Tool Kit\VertexTypes.h"

#include "..\Engine\Pipeline Stages\Shaders\Vertex Shader.h"
#include "..\Engine\Pipeline Stages\Shaders\Pixel Shader.h"

#include "..\Engine\Models\Mesh Base.h"

#include "..\Engine\Scene\Transforms\World Transforms.h"
#include "..\Engine\Scene\Transforms\View Transform.h"
#include "..\Engine\Scene\Transforms\Position.h"
#include "..\Engine\Scene\Transforms\Projection Transform.h"

#include "..\Engine\Lighting\Material.h"
#include "..\Engine\Lighting\Directional Light.h"

#include "..\Engine\Scene\Cameras\FirstPerson Camera.h"
#include "..\Engine\Scene\Input\Input Controller.h"
#include "..\Engine\Pipeline Resources\Textures\Texture2D.h"

#include "..\Engine\Models\Basic Shapes\Cubes.h"
#include "..\Engine\Models\Basic Shapes\Pyramids.h"
#include "..\Engine\Models\Generated Shapes\Grid.h"

#include "..\Models\Generated Shapes\GridNew.h"
#include "..\Models\Basic Shapes\CubeNew.h"


#include "RenderToTexture.h"

using namespace std;

using namespace DirectX;

using namespace Windows::System;//for keys


namespace vxe
{
	class Renderer3D : public RendererBase3D {

	public:
		Renderer3D::Renderer3D(InputController^ inputcontroller = nullptr) : RendererBase3D(inputcontroller)
		{
			DebugPrint(std::string("\t Renderer3D::Ctor... \n"));
		}

		virtual void CreateDeviceDependentResources() override;
		virtual void CreateWindowSizeDependentResources() override;
		virtual void ReleaseDeviceDependentResources() override;
		
		virtual void Render() override;

		void SetLighting();
		
		//void SetDepthBuffer();
		
		void Transform(float radians) override
		{
			DirectX::XMMATRIX R = DirectX::XMMatrixRotationY(radians);
			DirectX::XMMATRIX T = DirectX::XMMatrixTranslation(0.0f, 2.0f, 0.0f);
			DirectX::XMMATRIX S = DirectX::XMMatrixScaling(1.0f, 1.0f, 1.0f);

			_worldCube->Transform(S, R, T);

			T = DirectX::XMMatrixTranslation(0.0f, 1.0f, 3.0f);

			_worldCube1->Transform(S, R, T);

			T = DirectX::XMMatrixTranslation(4.0f, 0.0f, 0.0f);
			R = DirectX::XMMatrixRotationY(radians);

			_worldCube2->Transform(S, R, T);
			
			T = DirectX::XMMatrixTranslation(0.0f, -0.9f, 0.0f);
			R = DirectX::XMMatrixRotationY(0);

			_worldGrid->Transform(S, R, T);
			
		}


		void Renderer3D::SetChangeableLightProperties(const XMVECTORF32& lightEye, const XMVECTORF32& lightAt, const XMVECTORF32& lightUp)
		{
			auto device = m_deviceResources->GetD3DDevice();
			auto context = m_deviceResources->GetD3DDeviceContext();


			//Directional light
			
			_directional->SetComponents(_ambientLight, _diffuseLight, _specularLight, XMFLOAT3(lightEye.f));
			_directional->Update(context);
			_directional->GetConstantBuffer()->Bind(context, ProgrammableStage::PixelShaderStage, 2);

			//lightView
			
			_lightView->SetView(lightEye, lightAt, lightUp);
			_lightView->Update(context);
			_lightView->GetConstantBuffer()->Bind(context, ProgrammableStage::VertexShaderStage, 3);

			//light position
			
			_lightEye->SetPosition(lightEye);
			_lightEye->Update(context);
			_lightEye->GetConstantBuffer()->Bind(context, ProgrammableStage::VertexShaderStage, 5);
			
			
		}


		void Renderer3D::MoveLight()
		{

			auto position = _lightEye->GetValue();
			XMVECTORF32 lightEye = { position.x, position.y, position.z, position.w };
			XMVECTORF32 lightAt = { 0.0f, 0.0f, 0.0f, 0.0f };
			XMVECTORF32 lightUp = { 0.0f, 1.0f, 0.0f, 0.0f };
			//x-axis
			if (_inputcontroller->IsKeyDown(VirtualKey::Left)) {
				lightEye.v.m128_f32[0] -= 0.1f;

				SetChangeableLightProperties(lightEye, lightAt, lightUp);
			}

			if (_inputcontroller->IsKeyDown(VirtualKey::Right)) {
				lightEye.v.m128_f32[0] += 0.1f;

				SetChangeableLightProperties(lightEye, lightAt, lightUp);
			}
			//y-axis
			if (_inputcontroller->IsKeyDown(VirtualKey::Up)) {
				lightEye.v.m128_f32[1] += 0.1f;

				SetChangeableLightProperties(lightEye, lightAt, lightUp);
			}

			if (_inputcontroller->IsKeyDown(VirtualKey::Down)) {
				lightEye.v.m128_f32[1] -= 0.1f;

				SetChangeableLightProperties(lightEye, lightAt, lightUp);

			}
		}

		void Renderer3D::RenderingToTexture()
		{
			
			auto device = m_deviceResources->GetD3DDevice();
			auto context = m_deviceResources->GetD3DDeviceContext();

		
			_renderToTexture->SetRenderTarget(m_deviceResources);
			_renderToTexture->ClearRenderTarget(m_deviceResources, 0.0f, 0.0f, 0.0f, 1.0f);
			

			_vertexShaderDepth->Bind(context);
			_pixelShaderDepth->Bind(context);
			
			Draw(_modelCube2, _worldCube);
			Draw(_modelCube2, _worldCube1);
			Draw(_modelGrid, _worldGrid);
			
		}



		virtual void Update(DX::StepTimer const& timer) override
		{
			RendererBase3D::Update(timer);

			_camera->Update(_inputcontroller, timer);
		}

		
		void SetTexturing();


	private:
		

		void BindCamera()
		{
			auto context = m_deviceResources->GetD3DDeviceContext();

			_camera->UpdateEye(context);
			_camera->BindEye(context, ProgrammableStage::PixelShaderStage, 1);

			_camera->UpdateView(context);
			_camera->BindView(context, ProgrammableStage::VertexShaderStage, 1);

		}

		template <typename T, typename U>
		void Draw(std::shared_ptr<MeshBase<T, U>>& mesh, std::shared_ptr<WorldTransforms>& world, bool indexed = true)
		{
			auto context = m_deviceResources->GetD3DDeviceContext();

			world->Update(context);
			world->GetConstantBuffer()->Bind(context);

			mesh->BindVertexBuffer(context);
			mesh->BindIndexBuffer(context);

			if (indexed) mesh->DrawIndexed(context);
			else mesh->Draw(context);

		}

	private:

		//light values
		XMFLOAT4 _ambientLight = XMFLOAT4(0.5f, 0.5f, 0.6f, 1.0f);
		XMFLOAT4 _diffuseLight = XMFLOAT4(0.3f, 0.3f, 0.3f, 1.0f);
		XMFLOAT4 _specularLight = XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f);

		//shaders
		std::shared_ptr<VertexShader<DirectX::VertexPositionNormalTexture>> _vertexshader;
		std::shared_ptr<PixelShader> _pixelshader;

		std::shared_ptr<VertexShader<DirectX::VertexPosition>> _vertexShaderDepth;
		std::shared_ptr<PixelShader> _pixelShaderDepth;

		//models
		std::shared_ptr<MeshBase<DirectX::VertexPositionNormalTexture, unsigned short>> _modelCube;	
		std::shared_ptr<MeshBase<DirectX::VertexPositionNormalTexture, unsigned short>> _modelCube2;
		std::shared_ptr<MeshBase<DirectX::VertexPositionNormalTexture, unsigned short>> _modelGrid;
		
		std::shared_ptr<WorldTransforms> _worldCube;
		std::shared_ptr<WorldTransforms> _worldCube1;
		std::shared_ptr<WorldTransforms> _worldCube2;
		std::shared_ptr<WorldTransforms> _worldGrid;

		std::shared_ptr<FirstPersonCamera> _camera;

		std::shared_ptr<ViewTransform> _lightView;
		std::shared_ptr<Position> _lightEye;
		std::shared_ptr<ProjectionTransform> _lightProjection;
		std::shared_ptr<DirectionalLight> _directional;

		Microsoft::WRL::ComPtr<ID3D11Buffer> _textureSizeBuffer;
		
		std::shared_ptr<Material> _materialIce;
		std::shared_ptr<Material> _materialWall;
		std::shared_ptr<Material> _materialMetal;

		std::shared_ptr<Texture2D> _textureMetal;
		std::shared_ptr<Texture2D> _textureIce;
		std::shared_ptr<Texture2D> _textureWall;

		ID3D11ShaderResourceView* _depthMapTexture;

		
		std::shared_ptr<RenderTextureClass> _renderToTexture;

		Handedness _handedness = Handedness::LeftHanded;
		Windows::Foundation::Size _outputSize;
		std::vector < std::shared_ptr<WorldTransforms >> _worlds;
		std::vector < std::shared_ptr<MeshBase<VertexPositionNormalTexture, unsigned short >>> _models;
	};

}