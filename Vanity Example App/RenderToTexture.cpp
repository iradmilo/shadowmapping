#include "pch.h"
#include "RenderToTexture.h"

using namespace DirectX;
//using namespace vxe;

RenderTextureClass::RenderTextureClass()
{
	m_renderTargetTexture = nullptr;
	m_renderTargetView = nullptr;
	m_shaderResourceView = nullptr;
	m_depthStencilBuffer = nullptr;
	m_depthStencilView = nullptr;
	m_context = nullptr;
	m_device = nullptr;
	m_shadowTexture = nullptr;
	pShadowMapSRView = nullptr;

}


void RenderTextureClass::Initialize(std::shared_ptr<DX::DeviceResources> deviceResources, float textureWidth, float textureHeight)
{
	
	//render target texture
	m_device = deviceResources->GetD3DDevice();
	m_context = deviceResources->GetD3DDeviceContext();


	//RT and SRV
	CD3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(textureDesc));

	textureDesc.Width = textureWidth;
	textureDesc.Height = textureHeight;
	textureDesc.MipLevels = 1;
	textureDesc.ArraySize = 1;
	textureDesc.Format = DXGI_FORMAT_R32_FLOAT;//DXGI_FORMAT_R32G32B32A32_FLOAT;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;

	// Create the render target texture.
	vxe::ThrowIfFailed(m_device->CreateTexture2D(&textureDesc, NULL, &m_renderTargetTexture), __FILEW__, __LINE__);

	//Create render target view

	CD3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;

	renderTargetViewDesc.Format = textureDesc.Format;
	renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	renderTargetViewDesc.Texture2D.MipSlice = 0;

	vxe::ThrowIfFailed(m_device->CreateRenderTargetView(m_renderTargetTexture.Get(), /*&renderTargetViewDesc*/NULL, &m_renderTargetView), __FILEW__, __LINE__);

	//Create shader resource view
	
	CD3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;

	shaderResourceViewDesc.Format = DXGI_FORMAT_R32_FLOAT;
	shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
	shaderResourceViewDesc.Texture2D.MipLevels = 1;

	vxe::ThrowIfFailed(m_device->CreateShaderResourceView(m_renderTargetTexture.Get(), &shaderResourceViewDesc, &m_shaderResourceView), __FILEW__, __LINE__);
	

	

	//DSV
	//depth buffer
	CD3D11_TEXTURE2D_DESC depthBufferDesc;
	ZeroMemory(&depthBufferDesc, sizeof(depthBufferDesc));

	depthBufferDesc.Width = textureWidth;
	depthBufferDesc.Height = textureHeight;
	depthBufferDesc.MipLevels = 1;
	depthBufferDesc.ArraySize = 1;
	depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthBufferDesc.SampleDesc.Count = 1;
	depthBufferDesc.SampleDesc.Quality = 0;
	depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthBufferDesc.CPUAccessFlags = 0;
	depthBufferDesc.MiscFlags = 0;

	vxe::ThrowIfFailed(m_device->CreateTexture2D(&depthBufferDesc, NULL, &m_depthStencilBuffer), __FILEW__, __LINE__);

	//depth stencil view
	CD3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));

	depthStencilViewDesc.Format = depthBufferDesc.Format;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Texture2D.MipSlice = 0;
	depthStencilViewDesc.Flags = 0;

	vxe::ThrowIfFailed(m_device->CreateDepthStencilView(m_depthStencilBuffer.Get(), &depthStencilViewDesc, &m_depthStencilView), __FILEW__, __LINE__);

	

	//VIEWPORT
	m_viewport.Width = textureWidth;
	m_viewport.Height = textureHeight;
	m_viewport.MinDepth = 0.0f;
	m_viewport.MaxDepth = 1.0f;
	m_viewport.TopLeftX = 0;
	m_viewport.TopLeftY = 0;
}


void RenderTextureClass::SetRenderTarget(std::shared_ptr<DX::DeviceResources> m_deviceResources)
{
	m_context = m_deviceResources->GetD3DDeviceContext();

	
	m_context->OMSetRenderTargets(1, m_renderTargetView.GetAddressOf(), m_depthStencilView.Get());
	m_context->RSSetViewports(1, &m_viewport);
}

void RenderTextureClass::ClearRenderTarget(std::shared_ptr<DX::DeviceResources> m_deviceResources, float red, float green, float blue, float alpha)
{
	m_context = m_deviceResources->GetD3DDeviceContext();
	float color[4];

	color[0] = red;
	color[1] = green;
	color[2] = blue;
	color[3] = alpha;

	m_context->ClearRenderTargetView(m_renderTargetView.Get(), color);
	m_context->ClearDepthStencilView(m_depthStencilView.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
	
}

ID3D11ShaderResourceView* RenderTextureClass::GetShaderResourceView()
{
	return m_shaderResourceView.Get();
}

ID3D11Texture2D* RenderTextureClass::GetRenderTargetTexture(){
	return m_renderTargetTexture.Get();
};

void RenderTextureClass::Reset()
{
	if (m_depthStencilView)
	{
		m_depthStencilView.Reset();

	}

	if (m_depthStencilBuffer)
	{
		m_depthStencilBuffer.Reset();

	}

	if (m_shaderResourceView)
	{
		m_shaderResourceView.Reset();

	}

	if (m_device)
	{
		m_device.Reset();

	}

	if (m_context)
	{
		m_context.Reset();

	}

	if (m_renderTargetView)
	{
		m_renderTargetView.Reset();

	}

	if (m_renderTargetTexture)
	{
		m_renderTargetTexture.Reset();

	}

	if (m_shadowTexture)
	{
		m_shadowTexture.Reset();

	}
	if (pShadowMapSRView)
	{
		pShadowMapSRView.Reset();

	}
	
}
