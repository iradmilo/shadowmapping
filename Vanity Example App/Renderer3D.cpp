
//
//	Renderer3D.cpp
//  Implementing a 3D renderer
//
//  � 2015 Vanity DirectX 11.2 Engine (VXE). Zoraja Consulting d.o.o. All rights reserved.
//

#include "pch.h"

#include "Renderer3D.h"


using namespace vxe;

using namespace concurrency;

using namespace Windows::Foundation;


void Renderer3D::CreateDeviceDependentResources()
{
	DebugPrint(string("\t Renderer3D::CreateDeviceDependentResources() ... \n"));
	
	auto device = m_deviceResources->GetD3DDevice();

	vector<task<void>> tasks;

	// shaders
	_vertexshader = make_shared<VertexShader<VertexPositionNormalTexture>>();
	auto vstask = _vertexshader->CreateAsync(device, L"Lighting VertexShader.cso");
	tasks.push_back(vstask);

	_pixelshader = make_shared<PixelShader>();
	tasks.push_back(_pixelshader->CreateAsync(device, L"Lighting PixelShader.cso"));

	_vertexShaderDepth = make_shared<VertexShader<VertexPosition>>();
	tasks.push_back(_vertexShaderDepth->CreateAsync(device, L"VertexShaderDepth.cso"));

	_pixelShaderDepth = make_shared<PixelShader>();
	tasks.push_back(_pixelShaderDepth->CreateAsync(device, L"PixelShaderDepth.cso"));

	//models
	_modelCube = make_shared<Cube<VertexPositionNormalTexture, unsigned short>>();
	tasks.push_back(_modelCube->CreateAsync(device));

	_modelCube2 = make_shared<CubeNew<VertexPositionNormalTexture, unsigned short>>();
	tasks.push_back(_modelCube2->CreateAsync(device));
	

	_modelGrid = make_shared<GridNew<VertexPositionNormalTexture, unsigned short>>(100.0f, 100.0f, 101, 101);
	tasks.push_back(_modelGrid->CreateAsync(device));

	//textures
	_textureIce = make_shared<Texture2D>(device, DXGI_FORMAT_R8G8B8A8_UNORM);
	tasks.push_back(_textureIce->CreateDDSAsync(m_deviceResources->GetD3DDeviceContext(), L"Assets\\ice.dds"));

	_textureWall = make_shared<Texture2D>(device, DXGI_FORMAT_R8G8B8A8_UNORM);
	tasks.push_back(_textureWall->CreateDDSAsync(m_deviceResources->GetD3DDeviceContext(), L"Assets\\wall01.dds"));

	_textureMetal = make_shared<Texture2D>(device, DXGI_FORMAT_R8G8B8A8_UNORM);
	tasks.push_back(_textureMetal->CreateDDSAsync(m_deviceResources->GetD3DDeviceContext(), L"Assets\\metal001.dds"));

	_worldCube = make_shared<WorldTransforms>(device);
	_worldCube1 = make_shared<WorldTransforms>(device);
	_worldCube2 = make_shared<WorldTransforms>(device);
	_worldGrid = make_shared<WorldTransforms>(device);

	when_all(tasks.begin(), tasks.end()).then([this, device]() {
		m_loadingComplete = true;
		DebugPrint(string("\t -- A lambda: Loading is complete! \n"));
		SetTexturing();
		
	});

	SetLighting();

}

void Renderer3D::SetTexturing()
{
	DebugPrint(string("\t Renderer3D::SetTexturing() ...\n"));

	auto context = m_deviceResources->GetD3DDeviceContext();
	auto device = m_deviceResources->GetD3DDevice();

	_textureIce->CreateSamplerState();
	_textureIce->BindSamplerState(context);

	_textureIce->CreateShaderResourceView();
	
	_textureWall->CreateShaderResourceView();

	_textureMetal->CreateShaderResourceView();

	ID3D11SamplerState* m_sampleStateClamp;

	CD3D11_SAMPLER_DESC desc(CD3D11_DEFAULT{});
	desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	desc.MipLODBias = 0.0f;
	desc.MaxAnisotropy = 1;
	desc.BorderColor[0] = 0;
	desc.BorderColor[1] = 0;
	desc.BorderColor[2] = 0;
	desc.BorderColor[3] = 0;
	desc.MinLOD = 0;
	desc.MaxLOD = D3D11_FLOAT32_MAX;
	desc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	desc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	desc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;

	device->CreateSamplerState(&desc, &m_sampleStateClamp);

	context->PSSetSamplers(1, 1, &m_sampleStateClamp);

	//Sampler State for PCF
	ID3D11SamplerState* m_sampleStateCompare;
	CD3D11_SAMPLER_DESC desc1(CD3D11_DEFAULT{});

	desc1.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT;
	desc1.AddressU = D3D11_TEXTURE_ADDRESS_MIRROR;
	desc1.AddressV = D3D11_TEXTURE_ADDRESS_MIRROR;
	desc1.MaxLOD = D3D11_FLOAT_TO_SRGB_SCALE_1;
	desc1.ComparisonFunc = D3D11_COMPARISON_LESS_EQUAL;

	device->CreateSamplerState(&desc1, &m_sampleStateCompare);
	context->PSSetSamplers(2, 1, &m_sampleStateCompare);

}


void Renderer3D::SetLighting()
{
	DebugPrint(string("\t Renderer3D::SetLighting() ...\n"));

	auto device = m_deviceResources->GetD3DDevice();
	auto context = m_deviceResources->GetD3DDeviceContext();

	XMFLOAT4 ambientMaterialIce = XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f);
	XMFLOAT4 diffuseMaterialIce = XMFLOAT4(0.8f, 0.7f, 0.7f, 1.0f);
	XMFLOAT4 specularMaterialIce = XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f);
	XMFLOAT4 reflectionMaterialIce = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);

	XMFLOAT4 ambientMaterialWall = XMFLOAT4(0.7f, 0.8f, 0.8f, 1.0f);
	XMFLOAT4 diffuseMaterialWall = XMFLOAT4(0.8f, 0.7f, 0.7f, 1.0f);
	XMFLOAT4 specularMaterialWall = XMFLOAT4(0.3f, 0.3f, 0.1f, 1.0f);
	XMFLOAT4 reflectionMaterialWall = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);

	XMFLOAT4 ambientMaterialMetal = XMFLOAT4(0.7f, 0.8f, 0.8f, 1.0f);
	XMFLOAT4 diffuseMaterialMetal = XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f);
	XMFLOAT4 specularMaterialMetal = XMFLOAT4(0.3f, 0.3f, 0.3f, 1.0f);
	XMFLOAT4 reflectionMaterialMetal = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);

	_materialWall = make_shared<Material>(device);
	_materialWall->SetComponents(ambientMaterialWall, diffuseMaterialWall, specularMaterialWall, reflectionMaterialWall);

	_materialWall->Update(context);
	//_materialWall->GetConstantBuffer()->Bind(context, ProgrammableStage::PixelShaderStage);

	_materialIce = make_shared<Material>(device);
	_materialIce->SetComponents(ambientMaterialIce, diffuseMaterialIce, specularMaterialIce, reflectionMaterialIce);

	_materialIce->Update(context);
	//_materialIce->GetConstantBuffer()->Bind(context, ProgrammableStage::PixelShaderStage);

	_materialMetal = make_shared<Material>(device);
	_materialMetal->SetComponents(ambientMaterialMetal, diffuseMaterialMetal, specularMaterialMetal, reflectionMaterialMetal);

	_materialMetal->Update(context);
}


// Initializes view parameters when the window size changes
void Renderer3D::CreateWindowSizeDependentResources()
{
	
	_handedness = Handedness::LeftHanded;
	DebugPrint(string("\t Renderer3D::CreateWindowSizeDependentResources() ...\n"));

	auto device = m_deviceResources->GetD3DDevice();
	auto context = m_deviceResources->GetD3DDeviceContext();

	_outputSize = m_deviceResources->GetOutputSize();
	DebugPrint(string("\t\t Width:") + to_string(_outputSize.Width) + string("\n"));
	DebugPrint(string("\t\t Height:") + to_string(_outputSize.Height) + string("\n"));

	_camera = std::make_shared<FirstPersonCamera>(device);
	

	static const XMVECTORF32 eye = { 0.0f, 5.0f, -10.0f, 0.0f };
	//static const XMVECTORF32 eye = { 0.0f, 10.0f, 15.0f, 0.0f };
	static const XMVECTORF32 at = { 0.0f, 0.0f, 0.0f, 0.0f };
	static const XMVECTORF32 up = { 0.0f, 1.0f, 0.0f, 0.0f };


	_camera->InitializeView(eye, at, up);
	_camera->UpdateView(context);
	_camera->UpdateEye(context);


	float r = _outputSize.Width / _outputSize.Height;
	float fov = 70.0f * XM_PI / 180.0f;
	float n = 0.1f;
	float f = 1000.0f;

	if (r < 1.0f) { fov *= 2.0f; }

	
	XMFLOAT4X4 orientation = m_deviceResources->GetOrientationTransform3D();
	XMMATRIX orientationMatrix = DirectX::XMLoadFloat4x4(&orientation);


	_camera->SetProjection(orientationMatrix, fov, r, n, f);
	_camera->UpdateProjection(context);
	_camera->BindProjection(context, ProgrammableStage::VertexShaderStage, 2);



	static const XMVECTORF32 lightEye = { 0.0f, 7.0f, -7.0f, 0.0f };
	static const XMVECTORF32 lightAt = { 0.0f, 0.0f, 0.0f, 0.0f };
	static const XMVECTORF32 lightUp = { 0.0f, 1.0f, 0.0f, 0.0f };

	//lightView
	_lightView = make_shared<ViewTransform>(device);
	_lightView->SetView(lightEye, lightAt, lightUp);
	_lightView->Update(context);
	_lightView->GetConstantBuffer()->Bind(context, ProgrammableStage::VertexShaderStage, 3);


	//lightProjection
	float lightR = _outputSize.Width / _outputSize.Height;
	float lightFOV = 70.0f * XM_PI / 180.0f;
	float lightN = 1.0f;
	float lightF = 1000.0f;

	if (lightR < 1.0f) { lightFOV *= 2.0f; }

	//XMFLOAT4X4 orientation = m_deviceResources->GetOrientationTransform3D();
	//XMMATRIX orientationMatrix = DirectX::XMLoadFloat4x4(&orientation);

	_lightProjection = make_shared<ProjectionTransform>(device);
	_lightProjection->SetProjection(orientationMatrix, lightFOV, lightR, lightN, lightF);
	_lightProjection->Update(context);
	_lightProjection->GetConstantBuffer()->Bind(context, ProgrammableStage::VertexShaderStage, 4);


	//light position
	_lightEye = make_shared<Position>(device);
	_lightEye->SetPosition(lightEye);
	_lightEye->Update(context);
	_lightEye->GetConstantBuffer()->Bind(context, ProgrammableStage::VertexShaderStage, 5);

	_directional = make_shared<DirectionalLight>(device);

	
	//XMFLOAT3 direction = XMFLOAT3(lightEye.f);

	XMVECTOR d = _lightEye->GetPosition();
	XMFLOAT3 direction = XMFLOAT3(XMVectorGetX(d), XMVectorGetY(d), XMVectorGetZ(d));
	_directional->SetComponents(_ambientLight, _diffuseLight, _specularLight, direction);
	_directional->Update(context);
	_directional->GetConstantBuffer()->Bind(context, ProgrammableStage::PixelShaderStage, 2);

	_renderToTexture = make_shared<RenderTextureClass>();
	_renderToTexture->Initialize(m_deviceResources, _outputSize.Width, _outputSize.Height);

	//cbuffer shadow map size
	struct VS_CONSTANT_BUFFER
	{
		float textureWidth;
		float textureHeight;
	};

	VS_CONSTANT_BUFFER sizeBuffer;
	sizeBuffer.textureWidth = _outputSize.Width;
	sizeBuffer.textureHeight = _outputSize.Height;

	//constant buffer must be multiple of 16 bytes
	unsigned size = (16 * ((sizeof(float) + 15) / 16));

	CD3D11_BUFFER_DESC desc(size, D3D11_BIND_CONSTANT_BUFFER, D3D11_USAGE_DEFAULT);

	D3D11_SUBRESOURCE_DATA data = { 0 };
	data.pSysMem = &sizeBuffer;
	data.SysMemPitch = 0;
	data.SysMemSlicePitch = 0;

	ThrowIfFailed(device->CreateBuffer(
		&desc,
		&data,
		_textureSizeBuffer.ReleaseAndGetAddressOf()), __FILEW__, __LINE__);


	context->PSSetConstantBuffers(3, 1, _textureSizeBuffer.GetAddressOf());

}

// Renders one frame using the vertex and pixel shaders.
void Renderer3D::Render()
{
	
	// Loading is asynchronous. Only draw geometry after it's loaded.
	if (!m_loadingComplete) { return; }

	auto context = m_deviceResources->GetD3DDeviceContext();
	auto device = m_deviceResources->GetD3DDevice();

	ID3D11ShaderResourceView* null[] = { nullptr };

	
	RenderingToTexture();


	ID3D11RenderTargetView *const targets[1] = { m_deviceResources->GetBackBufferRenderTargetView() };
	context->OMSetRenderTargets(1, targets, m_deviceResources->GetDepthStencilView());
	context->RSSetViewports(1, &m_deviceResources->GetScreenViewport());

	CD3D11_RENDER_TARGET_VIEW_DESC desc;
	targets[0]->GetDesc(&desc);


	_depthMapTexture = _renderToTexture->GetShaderResourceView();
	//auto texture = _renderToTexture->GetRenderTargetTexture();
	
	CD3D11_SHADER_RESOURCE_VIEW_DESC desc1;
	_depthMapTexture->GetDesc(&desc1);

	context->PSSetShaderResources(1, 1, &_depthMapTexture);
	
	

	XMVECTORF32 color = { 0.14f, 0.14f, 0.14f, 1.0f };
	context->ClearRenderTargetView(m_deviceResources->GetBackBufferRenderTargetView(), color);
	context->ClearDepthStencilView(m_deviceResources->GetDepthStencilView(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
	

	_vertexshader->Bind(context);
	_pixelshader->Bind(context); 

	
	//Draw cube ice
	_materialIce->GetConstantBuffer()->Bind(context, ProgrammableStage::PixelShaderStage);
	_textureIce->BindShaderResourceView(context);
	Draw(_modelCube2, _worldCube);

	//Draw cube metal
	_materialMetal->GetConstantBuffer()->Bind(context, ProgrammableStage::PixelShaderStage);
	_textureMetal->BindShaderResourceView(context);
	Draw(_modelCube2, _worldCube1);

	//Draw floor
	_materialWall->GetConstantBuffer()->Bind(context, ProgrammableStage::PixelShaderStage);
	_textureWall->BindShaderResourceView(context); 
	Draw(_modelGrid, _worldGrid);

	BindCamera();
	MoveLight();
	

	
	m_deviceResources->SetRasterizerState();
	context->PSSetShaderResources(1, 1, null);
}

void Renderer3D::ReleaseDeviceDependentResources()
{
	DebugPrint(string("\t Renderer3D::ReleaseDeviceDependentResources() ... \n"));

	m_loadingComplete = false;

	_vertexshader->Reset();
	_pixelshader->Reset();
	_vertexShaderDepth->Reset();
	_pixelShaderDepth->Reset();
	
	_textureIce->Reset();
	_textureWall->Reset();
	_textureMetal->Reset();

	_modelCube2->Reset();
	_modelCube->Reset();
	_modelGrid->Reset();


	_worldCube->Reset();
	_worldCube1->Reset();
	_worldCube2->Reset();
	_worldGrid->Reset();

	_lightView->Reset();
	_lightEye->Reset();
	_lightProjection->Reset();

	_materialWall->Reset();
	_materialIce->Reset();
	_materialMetal->Reset();

	_directional->Reset();

	_camera->Reset();

	_textureSizeBuffer.Reset();

	_renderToTexture->Reset();
}